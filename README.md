# Rest API (Node JS + MongoDB)

Test Application of a REST Api. It is developed with Node + MongoDB in cloud (Atlas Mongodb)

## Available Scripts

In the project directory, you can run:

### `npm install`

Run this script to install all the dependencies

### `npm start`

Runs the service in the development mode.<br>
Open [http://localhost:3500](http://localhost:3500) to view it in the browser.

