const { Response } = require('../../utils');
const { collections } = require('../../database');
let appDB;

function router(app, db) {
  appDB = db;
  app
    .route('/api/user')
    .get(getUsers)
    .post(createUser);
}

async function getUsers(req, res) {
  const { name } = req.query;

  try {
    const result = name ? await appDB.findOne(collections.Users, { name }) : await appDB.find(collections.Users, {});
    if(!result) {
      res
          .status(Response.NotFoundErr.response)
          .send({ ...Response.NotFoundErr});
    } else {
      res
          .status(Response.Success.response)
          .send(result);
    }

  } catch (e) {
    res.status(Response.InternalError.response).send(Response.InternalError);
  }


}

async function createUser(req, res) {
  const { name, email, bags, level = 0 } = req.body;
  if (!name || !email || !bags ) {
    res.status(Response.BadReqErr.response).send(Response.BadReqErr);
    return;
  }
  try {
    await appDB.insertOne(collections.Users, { name, email, bags, level });
    res.status(Response.Success.response).send(Response.Success);
  } catch (e) {
    switch (e.code) {
      case 11000:
        res
          .status(Response.DuplicatedUser.response)
          .send(Response.DuplicatedUser);
        break;
      default:
        res
          .status(Response.InternalError.response)
          .send(Response.InternalError);
        break;
    }
  }
}

module.exports = router;
