// API endpoints definition
const userRoutes = require('./User');

// Add all routes of the API
module.exports = (app, db) => {
  userRoutes(app, db);
};
