const { Response } = require('../../utils');
let appDB;

function router(app, db) {
  appDB = db;
  app
    .route('/api/questionnaire')
    .get(getQuestionnaire)
    .post(createQuestionnaire);

  app
    .route('/api/answer')
    .get(getQuestionnaireAnswers)
    .post(answerQuestionnaire);
}

async function getQuestionnaire(req, res) {
  const { questionnaire } = req.query;
  if (!questionnaire) {
    res.status(Response.BadReqErr.response).send(Response.BadReqErr);
    return;
  }
  try {
    const result = await appDB.findOne(appDB.collections.Questionnaire, {
      questionnaire,
    });
    if (!result) {
      res.status(Response.NotFoundErr.response).send(Response.NotFoundErr);
    } else {
      res.status(Response.Success.response).send(result);
    }
  } catch (e) {
    res.status(Response.InternalError.response).send(Response.InternalError);
  }
}

async function createQuestionnaire(req, res) {
  const { user, questionnaire, questions } = req.body;
  if (!user || !questionnaire || !questions) {
    res.status(Response.BadReqErr.response).send(Response.BadReqErr);
    return;
  }

  try {
    const result = await appDB.findOne(appDB.collections.User, { user });
    if (!result) {
      res.status(Response.NotFoundErr.response).send(Response.NotFoundErr);
      return;
    }
    if (result.role !== appDB.roles.admin) {
      res.status(Response.NotAuthorized.response).send(Response.NotAuthorized);
    } else {
      await appDB.insertOne(appDB.collections.Questionnaire, {
        questionnaire,
        questions,
      });
      res.status(Response.Success.response).send(Response.Success);
    }
  } catch (e) {
    res.status(Response.InternalError.response).send(Response.InternalError);
  }
}

async function getQuestionnaireAnswers(req, res) {
  // User to check if it is an admin
  const { user } = req.query;
  if (!user) {
    res.status(Response.BadReqErr.response).send(Response.BadReqErr);
    return;
  }
  try {
    const result = await appDB.findOne(appDB.collections.User, { user });
    if (!result) {
      res.status(Response.NotFoundErr.response).send(Response.NotFoundErr);
      return;
    }
    if (result.role !== appDB.roles.admin) {
      res.status(Response.NotAuthorized.response).send(Response.NotAuthorized);
    } else {
      const answers = await appDB.find(appDB.collections.Answers);
      if (!answers) {
        res.status(Response.NotFoundErr.response).send(Response.NotFoundErr);
      } else {
        res.status(Response.Success.response).send(answers);
      }
    }
  } catch (e) {
    res.status(Response.InternalError.response).send(Response.InternalError);
  }
}

async function answerQuestionnaire(req, res) {
  const { user, questionnaire, answers } = req.body;
  if (!user || !questionnaire || !answers) {
    res.status(Response.BadReqErr.response).send(Response.BadReqErr);
  }
  try {
    const [userResult, questionnaireResult] = await Promise.all([
      appDB.findOne(appDB.collections.User, { user }),
      appDB.findOne(appDB.collections.Questionnaire, { questionnaire }),
    ]);

    // If the user or the questionnaire dont exist return Not Found error
    if (!userResult || !questionnaireResult) {
      res.status(Response.NotFoundErr.response).send(Response.NotFoundErr);
      return;
    }

    const prevAnswers = await appDB.findOne(appDB.collections.Answers, {
      user,
    });
    if (prevAnswers) {
      // Case 1: User with previous answers
      prevAnswers.questionnaires = prevAnswers.questionnaires.filter(
        q => q.questionnaire !== questionnaire,
      );
      prevAnswers.questionnaires.push({ questionnaire, answers });

      await appDB.updateOne(appDB.collections.Answers, { user }, prevAnswers);
    } else {
      // Case 2: User without previousAnswers
      await appDB.insertOne(appDB.collections.Answers, {
        user,
        questionnaires: [{ questionnaire, answers }],
        creationDate: Date.now(),
      });
    }
    res.status(Response.Success.response).send(Response.Success);
  } catch (e) {
    res.status(Response.InternalError.response).send(Response.InternalError);
  }
}

module.exports = router;
