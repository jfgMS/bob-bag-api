const Success = { response: 200, msg: 'Successfully' };
const BadReqErr = { response: 400, msg: 'Bad request' };
const DuplicatedUser = { response: 400, msg: 'User already exists' };
const NotAuthorized = { response: 401, msg: 'Not authorized' };
const NotFoundErr = { response: 404, msg: 'Not found' };
const InternalError = { response: 500, msg: 'Internal server error' };

module.exports = {
  Success,
  BadReqErr,
  DuplicatedUser,
  NotAuthorized,
  NotFoundErr,
  InternalError,
};
