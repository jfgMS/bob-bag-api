const { MongoClient } = require('mongodb');
const uri = // TODO: This must be loaded by config or enviroment variables
  'mongodb+srv://MasterUser:BobMaster1*@oupe-cluster0-6p3f4.mongodb.net/test?retryWrites=true';
const client = new MongoClient(uri, { useNewUrlParser: true });
let db;
const { dbName, collections } = require('./dbConfig');
async function connect() {
  client.connect(err => {
    if (err) {
      console.log('Something ocurred...', err);
      throw err;
    } else {
      console.log('Connected...');
    }
    db = client.db(dbName);
  });
}

async function findOne(collection, query) {
  if (!db) {
    throw new Error('DB not available');
  }
  return db.collection(collection).findOne(query);
}

async function find(collection, query) {
  if (!db) {
    throw new Error('DB not available');
  }
  return db
    .collection(collection)
    .find(query)
    .toArray();
}

async function insertOne(collection, query) {
  if (!db) {
    throw new Error('DB not available');
  }
  return db.collection(collection).insertOne(query);
}

async function updateOne(collection, keyField, fields) {
  if (!db) {
    throw new Error('DB not available');
  }
  return db
    .collection(collection)
    .updateOne(keyField, { $set: { ...fields } }, { upsert: true });
}

module.exports = {
  collections,
  connect,
  findOne,
  find,
  insertOne,
  updateOne,
};
